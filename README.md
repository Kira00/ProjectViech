This is a student-project made for the SAE Munich in Unity3D.

Target of the project was to recreate Bomberman as a First-Person game with a 
slight horror survival touch.

The goal for the player is to travel through the level and reach a lightbeam.
As means of defense the player has a variaty of bombs with different effects 
and costs.
The currency used for the bombs is energy that is refilled by walking around.